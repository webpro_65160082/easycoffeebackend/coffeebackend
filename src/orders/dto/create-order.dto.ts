export class CreateOrderDto {
  orderItems: {
    productID: number;
    qty: number;
  }[];
  userID: number;
}
