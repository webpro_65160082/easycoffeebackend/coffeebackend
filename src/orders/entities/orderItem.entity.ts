/* eslint-disable prettier/prettier */
import { Product } from 'src/products/entities/product.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Entity,
} from 'typeorm';
import { Order } from './order.entity';
@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @CreateDateColumn()
  create: Date;

  @UpdateDateColumn()
  update: Date;

  @ManyToOne(() => Order, (order) => order.orderItems, { onDelete: 'CASCADE' })
  order: Order[];

  @ManyToOne(() => Product, (product) => product.orderItems)
  product: Product;
}
